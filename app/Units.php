<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Units extends Model
{
   protected $table='units';
    protected $fillable = ['unit','full_unit','factor','updated_at'];
}
