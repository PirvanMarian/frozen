<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Units;
use App\Unit;
use App\Values;
 use DB;

class ConvertorController extends Controller
{
    public function index(Request $request)
    {
    	$convertors = Units::get();


	//dd($request->all());
    	    	
    	return view('index',compact('convertors'));

    }

    public function addUnit(Request $request){
    	$unit =  new Units;

		$unitM= $request->input('unit');
		$valueFactor="";
		$valueFullUnit ="";
  		
  		if($unitM == 'm')
  		{
			$valueFactor =1;
			$valueFullUnit="Meter";
		}
		else if($unitM ='dm')
		{
			$valueFactor =0.1;
			$valueFullUnit="Deci Meter";
		}
		else if($unitM == 'cm')
		{
			 $valueFactor =0.01;
			 $valueFullUnit="Centimeter";
		}
		else if($unitM == 'km')
		{
			$valueFactor =1000;
			$valueFullUnit="Kilometer";
		}
		else if($unitM == 'ft')
		{
			  $valueFactor =0.3048;
			  $valueFullUnit="Foot";
		}
		else if($unitM == 'mile')
		{
			 $valueFactor =1609.344;
			  $valueFullUnit="Mile";
		}
		else if($unitM == 'yd')
		{
			$valueFactor =0.9144;
			$valueFullUnit="Yard";
		}


	


    	$unitValue = $request->input('unit');

        $unit->unit =$unitValue;
        $unit->full_unit =$valueFullUnit;
        $unit->factor =$valueFactor;

        $unit->save();    

        dd($request->all());
    }


    public function factor(Request $request)
    {

    $unitm =$request->input('theUnit');

    $factor = $this->lengthFactor();

    return $factor;

    /*if($unitm ='Length')
    {
    	return 'plm';
    }

    else {
    	dd('dd');
    }*/

    }	

    public function lengthFactor () {

    	if($unitM == 'm'){
			$valueFactor =1;
		}
		else if($unitM == 'cm')
		{
			$valueFactor =.01;
		}
		else if($unitM == 'km')
		{
			$valueFactor =1000;
		}
		else if($unitM == 'ft')
		{
			$valueFactor =.3048;
		}
		else if($unitM == 'mile')
		{
			$valueFactor =1609.344;
		}
		else if($unitM == 'yd')
		{
			$valueFactor =.9144;
		}

	
		return $valueFactor;
    }

    

    public function insert(Request $request) 
    {
    	$factor= '';
    	$unit = $request->input('unit');
		$unitM ='';
		$valueFactor ='';

    	$menu =$request->input('unit_menu');


    	if($unit == 'm'){
			$valueFactor =1;
		}
		else if($unit == 'cm')
		{
			$valueFactor =0.01;
		}
		else if($unit == 'km')
		{
			$valueFactor =1000;
		}
		else if($unit == 'ft')
		{
			$valueFactor =.3048;
		}
		else if($unit == 'mile')
		{
			$valueFactor =1609.344;
		}
		else if($unit == 'yd')
		{
			$valueFactor =.9144;
		}

		return $valueFactor;

		//return $valueFactor;

		/*if($menu == 'Length'){
			dd(1);
			}
		else {
		dd(2);
		}*/
    	//dd($menu);
    	//$convertors = Unit::create(['unit' => 'Flight 10','full_unit'=> 'Default' ]);
    	$data=array(
    		'unit'=>$unit,
    		'full_unit'=>'def' , 
    		'factor'=> $valueFactor
    	);

		DB::table('units')->insert($data);
    
    }

    public function addCalculus (Request $request )
    {


    	$value = new Values;


    	$value->save();
    }
}
